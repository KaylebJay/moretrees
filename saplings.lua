-- sapling growth
-- these tables only affect hand-placed saplings
-- mapgen-placed always use their biome def settings, which are much more
-- limited, in the interest of speed.

local dirt_surfaces = {
	set = true,
	["default:dirt"] = true,
	["default:dirt_with_grass"] = true,
	["default:dirt_with_dry_grass"] = true,
	["default:dirt_with_coniferous_litter"] = true,
	["default:dirt_with_rainforest_litter"] = true,
	["woodsoils:dirt_with_leaves_1"] = true,
	["woodsoils:dirt_with_leaves_2"] = true,
	["woodsoils:grass_with_leaves_1"] = true,
	["woodsoils:grass_with_leaves_2"] = true
}

local conifer_surfaces =  {
	set = true,
	["default:dirt"] = true,
	["default:dirt_with_grass"] = true,
	["default:dirt_with_dry_grass"] = true,
	["default:dirt_with_coniferous_litter"] = true,
	["default:dirt_with_rainforest_litter"] = true,
	["woodsoils:dirt_with_leaves_1"] = true,
	["woodsoils:dirt_with_leaves_2"] = true,
	["woodsoils:grass_with_leaves_1"] = true,
	["woodsoils:grass_with_leaves_2"] = true,
	["default:dirt_with_snow"] = true
}

local sand_surfaces = {
	set = true,
	["default:sand"] = true,
	["default:desert_sand"] = true,
	["cottages:loam"] = true,
	-- note, no silver sand here.
	-- too cold for a palm, too... well... sandy for anything else.
}

local grow_ids = {}
local grow_ids_ongen = {}
local grow_surfaces = {}
local grow_functions = {}
local grow_nodes = {}
local grow_nodes_ongen = {}
local tree_id = 0

for i in ipairs(moretrees.treelist) do
	tree_id = tree_id + 1
	local treename = moretrees.treelist[i][1]
	local tree_model = treename.."_model"
	local tree_biome = treename.."_biome"
	local surfaces
	local grow_function = moretrees[tree_model]

	if treename == "spruce"
	  or treename == "fir"
	  or treename == "cedar"
	  or treename == "pine" then
		surfaces = conifer_surfaces
	elseif string.find(treename, "palm") then
		surfaces = sand_surfaces
	else
		surfaces = dirt_surfaces
	end

	if treename == "spruce"
	  or treename == "fir"
	  or treename == "birch"
	  or treename == "jungletree" then
		grow_function = "moretrees.grow_"..treename
	end
	
	grow_nodes[#grow_nodes + 1] = "moretrees:"..treename.."_sapling"
	grow_nodes_ongen[#grow_nodes_ongen + 1] = "moretrees:"..treename.."_sapling_ongen"
	grow_ids["moretrees:"..treename.."_sapling"] = tree_id
	grow_ids_ongen["moretrees:"..treename.."_sapling_ongen"] = tree_id
	grow_surfaces[tree_id] = surfaces
	grow_functions[tree_id] = grow_function

	biome_lib:dbg(dump(moretrees[tree_biome].surface))
end

biome_lib:grow_plants_batch({
	grow_delay = moretrees.sapling_interval,
	grow_chance = moretrees.sapling_chance,
	grow_plant = grow_nodes,
	grow_nodes = grow_surfaces,
	grow_function = grow_functions,
	ids = grow_ids,
})

biome_lib:grow_plants_batch({
	make_lbm = true,
	lbm_name = "moretrees:grow_trees_ongen_lbm",
	grow_plant = grow_nodes_ongen,
	grow_nodes = grow_surfaces,
	grow_function = grow_functions,
	ids = grow_ids_ongen,
})

